# Ingresses

Pods. Deployments. Services. And yet even with all of this, we *still* haven't solved the problem of *getting some piece of content to some user's web browser*!

Without going into the details about *ingress controllers*, an *ingress* resource just says that you want a service to be available outside of your cluster at some URL. A user (whether browser or otherwise) should be able to *ingress* into the cluster and reach your service as if it was just some website or web API--because that's exactly all it is, after all!

If your cluster's url is `https://mycluster.somesite.com`, then your services will be one or more levels deeper, e.g. `https://myservice.mycluster.somesite.com`.

## Create an Ingress

This one is relatively straightforward if the cluster is configured properly. Rancher handles a lot of the behind-the-scenes magic, but the gist is that there is an Nginx proxy at the cluster level that listens for requests and forwards them to IPs within the cluster based on the *ingress* resources that have been defined.

Put the following into `myfirstingress.yml`:

```yml
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  name: myfirstingress
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /
spec:
  tls: # only include the tls section if the cluster is set up with wildcard tls (ask Jon)
    - hosts:
      - <pick-a-name>.meerkat.act3-ace.ai
  rules:
    - host: <pick-a-name>.meerkat.act3-ace.ai
      http:
        paths:
        - path: /
          backend:
            serviceName: myfirstservice
            servicePort: 80
```

Unfortunately there's no VS Code snippet for *ingress* right now, so you'll have to copy/paste or do it by hand.

Once that's done, apply it as usual:

```bash
kubectl -n <your-ns-name> apply -f myfirstingress.yml
```

Then use your browser to go to the URL that you chose earlier. If the stars align juuust right, you'll see the Nginx welcome page!

Pods, deployments, services, and ingresses. The basic building blocks for boring services that never do anything different and never store any data. If all we ever wanted to do was show the Nginx welcome page at different URLs, making it super highly-available and fault-tolerant, we'd be all set.

In the real world, however, people are interested in services that actually store and modify data. We should be able to serve some static web pages *other* than the default page--that would at least bring us on par with web hosters of the mid-90s! And if we get *really* ambitious, we may even want to receive and store data *dynamically* and have it *persist* even when pods die from various causes.

For all of that, we need volumes.
