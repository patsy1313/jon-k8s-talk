# Pods

## Create a pod

The term *pod* is k8s-ese for *container*, basically. Technically, a pod can have multiple containers in it, but usually it just has one. Let's create a pod! Put the following into a file named `myfirstpod.yml`:

```yml
apiVersion: v1
kind: Pod
metadata:
  name: myfirstpod
  labels:
    name: myfirstpod
spec:
  containers:
  - name: myfirstpod
    image: nginx
    resources:
      limits:
        memory: "128Mi"
        cpu: "500m"
    ports:
      - containerPort: 80
```

(Bonus points if you used the *pod* snippet from the Kubernetes VS Code plugin.)

Then create that pod by *applying* the file to the cluster:

```bash
kubectl -n <your-ns-name> apply -f myfirstpod.yml # use tab completion!
```

Now check to see if it's running...

```bash
kubectl -n <your-ns-name> get pods # tab complete is your friend!
```

The status might say `ContainerCreating` for a little while, but eventually it will say `Running` if you keep checking the status.

Great! It's running! So how do we use it?

## Port-forward the pod to your local machine

The `nginx` pod is listening on port 80 (extra credit: what IP does it have in the cluster? Use `kubectl describe`), but you can't just get to any IP and port in the cluster.

Well, it turns out you actually *can* do this with `kubectl port-forward`, but you should *only do this for testing and debugging*. We'll talk about a better way some other time. Here's the `kubectl port-forward` syntax:

```bash
kubectl -n <your-ns-name> port-forward <your-pod-name> <local-port>:<pod-port>
```

It creates a magical wormhole that connects the pod's port to a port on your local machine! Try it:

```bash
kubectl -n <your-ns-name> port-forward myfirstpod 8080:80
```

Then use your browser to navigate to `http://localhost:8080`. You connected to the pod running in the cluster!

That pod isn't particularly useful, so let's get rid of it:

```bash
kubectl -n <your-ns-name> delete -f myfirstpod.yml
```

Now your namespace is empty and clean again. Nice and simple.

There's a drawback to this simplicity, though. It's fragile! You deleted the pod yourself when you were done with it, which is fine, but what if something had gone wrong? What if the node that the pod was running on caught fire? The pod would be gone, and you would have to re-create it manually! Fortunately, Kubernetes has plans to keep your pods running even when bad things happen, and it involves *deployments*.
